import datetime
import logging
import socket
import requests

from flask import Flask, request
from google.cloud import datastore


app = Flask(__name__)


def is_ipv6(addr):
    """Checks if a given address is an IPv6 address."""
    try:
        socket.inet_pton(socket.AF_INET6, addr)
        return True
    except socket.error:
        return False


@app.route('/')
def index():
    ds = datastore.Client()
    user_ip = request.remote_addr

    freegeoip = "http://freegeoip.net/json"
    geo_r = requests.get(freegeoip)
    geo_json = geo_r.json()

    # Keep only the first two octets of the IP address.
    if is_ipv6(user_ip):
        user_ip = ':'.join(user_ip.split(':')[:2])
    else:
        user_ip = '.'.join(user_ip.split('.')[:2])

    entity = datastore.Entity(key=ds.key('location'))
    entity.update({
        'user_ip': user_ip,
        'timestamp' : datetime.datetime.utcnow(),
        'latitude' : geo_json['latitude'],
        'longitude' : geo_json['longitude']
    })

    ds.put(entity)
    query = ds.query(kind='location', order=('-timestamp',))

    results = [
        'Time: {timestamp} Addr: {user_ip} Latitude: {latitude} Longitude: {longitude}'.format(**x)
        for x in query.fetch(limit=10)]

    output = 'Last 10 visits:\n{}'.format('\n'.join(results))

    def publish_message(topic_name, data):
        pubsub_client = pubsub.Client()
        topic = pubsub_client.topic("mytopic")

        data = results.encode('utf-8')
        message_id = topic.publish(data)


    return output, 200, {'Content-Type': 'text/plain; charset=utf-8'}
# [END example]


@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500


if __name__ == '__main__':
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine. See entrypoint in app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)